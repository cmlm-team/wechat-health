package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/autocode"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    autoCodeReq "github.com/flipped-aurora/gin-vue-admin/server/model/autocode/request"
)

type H_packageService struct {
}

// CreateH_package 创建H_package记录
// Author [piexlmax](https://github.com/piexlmax)
func (h_packageService *H_packageService) CreateH_package(h_package autocode.H_package) (err error) {
	err = global.GVA_DB.Create(&h_package).Error
	return err
}

// DeleteH_package 删除H_package记录
// Author [piexlmax](https://github.com/piexlmax)
func (h_packageService *H_packageService)DeleteH_package(h_package autocode.H_package) (err error) {
	err = global.GVA_DB.Delete(&h_package).Error
	return err
}

// DeleteH_packageByIds 批量删除H_package记录
// Author [piexlmax](https://github.com/piexlmax)
func (h_packageService *H_packageService)DeleteH_packageByIds(ids request.IdsReq) (err error) {
	err = global.GVA_DB.Delete(&[]autocode.H_package{},"id in ?",ids.Ids).Error
	return err
}

// UpdateH_package 更新H_package记录
// Author [piexlmax](https://github.com/piexlmax)
func (h_packageService *H_packageService)UpdateH_package(h_package autocode.H_package) (err error) {
	err = global.GVA_DB.Save(&h_package).Error
	return err
}

// GetH_package 根据id获取H_package记录
// Author [piexlmax](https://github.com/piexlmax)
func (h_packageService *H_packageService)GetH_package(id uint) (err error, h_package autocode.H_package) {
	err = global.GVA_DB.Where("id = ?", id).First(&h_package).Error
	return
}

// GetH_packageInfoList 分页获取H_package记录
// Author [piexlmax](https://github.com/piexlmax)
func (h_packageService *H_packageService)GetH_packageInfoList(info autoCodeReq.H_packageSearch) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.GVA_DB.Model(&autocode.H_package{})
    var h_packages []autocode.H_package
    // 如果有条件搜索 下方会自动创建搜索语句
    if info.Name != "" {
        db = db.Where("`name` LIKE ?","%"+ info.Name+"%")
    }
    if info.Date != "" {
        db = db.Where("`date` = ?",info.Date)
    }
    if info.Count != nil {
        db = db.Where("`count` = ?",info.Count)
    }
    if info.A_id != nil {
        db = db.Where("`a_id` = ?",info.A_id)
    }
	err = db.Count(&total).Error
	if err!=nil {
    	return
    }
	err = db.Limit(limit).Offset(offset).Find(&h_packages).Error
	return err, h_packages, total
}
