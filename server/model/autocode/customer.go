// 自动生成模板Customer
package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// Customer 结构体
// 如果含有time.Time 请自行import time包
type Customer struct {
      global.GVA_MODEL
      Name  string `json:"name" form:"name" gorm:"column:name;comment:姓名;type:varchar(20);"`
      Phone  string `json:"phone" form:"phone" gorm:"column:phone;comment:手机号;type:varchar(13);"`
}


