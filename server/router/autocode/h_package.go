package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type H_packageRouter struct {
}

// InitH_packageRouter 初始化 H_package 路由信息
func (s *H_packageRouter) InitH_packageRouter(Router *gin.RouterGroup) {
	h_packageRouter := Router.Group("h_package").Use(middleware.OperationRecord())
	h_packageRouterWithoutRecord := Router.Group("h_package")
	var h_packageApi = v1.ApiGroupApp.AutoCodeApiGroup.H_packageApi
	{
		h_packageRouter.POST("createH_package", h_packageApi.CreateH_package)   // 新建H_package
		h_packageRouter.DELETE("deleteH_package", h_packageApi.DeleteH_package) // 删除H_package
		h_packageRouter.DELETE("deleteH_packageByIds", h_packageApi.DeleteH_packageByIds) // 批量删除H_package
		h_packageRouter.PUT("updateH_package", h_packageApi.UpdateH_package)    // 更新H_package
	}
	{
		h_packageRouterWithoutRecord.GET("findH_package", h_packageApi.FindH_package)        // 根据ID获取H_package
		h_packageRouterWithoutRecord.GET("getH_packageList", h_packageApi.GetH_packageList)  // 获取H_package列表
	}
}
