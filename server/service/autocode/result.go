package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/autocode"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    autoCodeReq "github.com/flipped-aurora/gin-vue-admin/server/model/autocode/request"
)

type ResultService struct {
}

// CreateResult 创建Result记录
// Author [piexlmax](https://github.com/piexlmax)
func (resultService *ResultService) CreateResult(result autocode.Result) (err error) {
	err = global.GVA_DB.Create(&result).Error
	return err
}

// DeleteResult 删除Result记录
// Author [piexlmax](https://github.com/piexlmax)
func (resultService *ResultService)DeleteResult(result autocode.Result) (err error) {
	err = global.GVA_DB.Delete(&result).Error
	return err
}

// DeleteResultByIds 批量删除Result记录
// Author [piexlmax](https://github.com/piexlmax)
func (resultService *ResultService)DeleteResultByIds(ids request.IdsReq) (err error) {
	err = global.GVA_DB.Delete(&[]autocode.Result{},"id in ?",ids.Ids).Error
	return err
}

// UpdateResult 更新Result记录
// Author [piexlmax](https://github.com/piexlmax)
func (resultService *ResultService)UpdateResult(result autocode.Result) (err error) {
	err = global.GVA_DB.Save(&result).Error
	return err
}

// GetResult 根据id获取Result记录
// Author [piexlmax](https://github.com/piexlmax)
func (resultService *ResultService)GetResult(id uint) (err error, result autocode.Result) {
	err = global.GVA_DB.Where("id = ?", id).First(&result).Error
	return
}

// GetResultInfoList 分页获取Result记录
// Author [piexlmax](https://github.com/piexlmax)
func (resultService *ResultService)GetResultInfoList(info autoCodeReq.ResultSearch) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
    // 创建db
	db := global.GVA_DB.Model(&autocode.Result{})
    var results []autocode.Result
    // 如果有条件搜索 下方会自动创建搜索语句
	err = db.Count(&total).Error
	if err!=nil {
    	return
    }
	err = db.Limit(limit).Offset(offset).Find(&results).Error
	return err, results, total
}
