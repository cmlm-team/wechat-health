// 自动生成模板Result
package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// Result 结构体
// 如果含有time.Time 请自行import time包
type Result struct {
      global.GVA_MODEL
      C_id  *int `json:"c_id" form:"c_id" gorm:"column:c_id;comment:;type:bigint"`
      A_id  *int `json:"a_id" form:"a_id" gorm:"column:a_id;comment:管理员id;type:bigint"`
      File_url  string `json:"file_url" form:"file_url" gorm:"column:file_url;comment:;type:varchar(255);"`
      O_id  *int `json:"o_id" form:"o_id" gorm:"column:o_id;comment:订单id;type:bigint"`
}


