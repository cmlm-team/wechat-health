package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/autocode"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    autocodeReq "github.com/flipped-aurora/gin-vue-admin/server/model/autocode/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
)

type ResultApi struct {
}

var resultService = service.ServiceGroupApp.AutoCodeServiceGroup.ResultService


// CreateResult 创建Result
// @Tags Result
// @Summary 创建Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body autocode.Result true "创建Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /result/createResult [post]
func (resultApi *ResultApi) CreateResult(c *gin.Context) {
	var result autocode.Result
	_ = c.ShouldBindJSON(&result)
	if err := resultService.CreateResult(result); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteResult 删除Result
// @Tags Result
// @Summary 删除Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body autocode.Result true "删除Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /result/deleteResult [delete]
func (resultApi *ResultApi) DeleteResult(c *gin.Context) {
	var result autocode.Result
	_ = c.ShouldBindJSON(&result)
	if err := resultService.DeleteResult(result); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteResultByIds 批量删除Result
// @Tags Result
// @Summary 批量删除Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /result/deleteResultByIds [delete]
func (resultApi *ResultApi) DeleteResultByIds(c *gin.Context) {
	var IDS request.IdsReq
    _ = c.ShouldBindJSON(&IDS)
	if err := resultService.DeleteResultByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Any("err", err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateResult 更新Result
// @Tags Result
// @Summary 更新Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body autocode.Result true "更新Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /result/updateResult [put]
func (resultApi *ResultApi) UpdateResult(c *gin.Context) {
	var result autocode.Result
	_ = c.ShouldBindJSON(&result)
	if err := resultService.UpdateResult(result); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindResult 用id查询Result
// @Tags Result
// @Summary 用id查询Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query autocode.Result true "用id查询Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /result/findResult [get]
func (resultApi *ResultApi) FindResult(c *gin.Context) {
	var result autocode.Result
	_ = c.ShouldBindQuery(&result)
	if err, reresult := resultService.GetResult(result.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"reresult": reresult}, c)
	}
}

// GetResultList 分页获取Result列表
// @Tags Result
// @Summary 分页获取Result列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query autocodeReq.ResultSearch true "分页获取Result列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /result/getResultList [get]
func (resultApi *ResultApi) GetResultList(c *gin.Context) {
	var pageInfo autocodeReq.ResultSearch
	_ = c.ShouldBindQuery(&pageInfo)
	if err, list, total := resultService.GetResultInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
