package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type ResultRouter struct {
}

// InitResultRouter 初始化 Result 路由信息
func (s *ResultRouter) InitResultRouter(Router *gin.RouterGroup) {
	resultRouter := Router.Group("result").Use(middleware.OperationRecord())
	resultRouterWithoutRecord := Router.Group("result")
	var resultApi = v1.ApiGroupApp.AutoCodeApiGroup.ResultApi
	{
		resultRouter.POST("createResult", resultApi.CreateResult)   // 新建Result
		resultRouter.DELETE("deleteResult", resultApi.DeleteResult) // 删除Result
		resultRouter.DELETE("deleteResultByIds", resultApi.DeleteResultByIds) // 批量删除Result
		resultRouter.PUT("updateResult", resultApi.UpdateResult)    // 更新Result
	}
	{
		resultRouterWithoutRecord.GET("findResult", resultApi.FindResult)        // 根据ID获取Result
		resultRouterWithoutRecord.GET("getResultList", resultApi.GetResultList)  // 获取Result列表
	}
}
