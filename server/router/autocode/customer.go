package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type CustomerRouter struct {
}

// InitCustomerRouter 初始化 Customer 路由信息
func (s *CustomerRouter) InitCustomerRouter(Router *gin.RouterGroup) {
	customerRouter := Router.Group("customer").Use(middleware.OperationRecord())
	customerRouterWithoutRecord := Router.Group("customer")
	var customerApi = v1.ApiGroupApp.AutoCodeApiGroup.CustomerApi
	{
		customerRouter.POST("createCustomer", customerApi.CreateCustomer)   // 新建Customer
		customerRouter.DELETE("deleteCustomer", customerApi.DeleteCustomer) // 删除Customer
		customerRouter.DELETE("deleteCustomerByIds", customerApi.DeleteCustomerByIds) // 批量删除Customer
		customerRouter.PUT("updateCustomer", customerApi.UpdateCustomer)    // 更新Customer
	}
	{
		customerRouterWithoutRecord.GET("findCustomer", customerApi.FindCustomer)        // 根据ID获取Customer
		customerRouterWithoutRecord.GET("getCustomerList", customerApi.GetCustomerList)  // 获取Customer列表
	}
}
