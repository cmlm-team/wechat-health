// 自动生成模板H_package
package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// H_package 结构体
// 如果含有time.Time 请自行import time包
type H_package struct {
      global.GVA_MODEL
      Name  string `json:"name" form:"name" gorm:"column:name;comment:体检项目名称;type:varchar(20);"`
      Date  string `json:"date" form:"date" gorm:"column:date;comment:套餐预约日期;type:varchar(10);"`
      Count  *int `json:"count" form:"count" gorm:"column:count;comment:;type:int"`
      A_id  *int `json:"a_id" form:"a_id" gorm:"column:a_id;comment:管理员id;type:bigint"`
      Detail  string `json:"detail" form:"detail" gorm:"column:detail;comment:;type:text(255);"`
}


