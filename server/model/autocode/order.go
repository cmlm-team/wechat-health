// 自动生成模板Order
package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// Order 结构体
// 如果含有time.Time 请自行import time包
type Order struct {
      global.GVA_MODEL
      C_id  *int `json:"c_id" form:"c_id" gorm:"column:c_id;comment:客户id;type:bigint"`
      P_id  *int `json:"p_id" form:"p_id" gorm:"column:p_id;comment:;type:bigint"`
      R_id  *int `json:"r_id" form:"r_id" gorm:"column:r_id;comment:体检结果id;type:bigint"`
      Status  *int `json:"status" form:"status" gorm:"column:status;comment:0:未完成，1:完成未上传结果，2:完成;type:smallint"`
}


