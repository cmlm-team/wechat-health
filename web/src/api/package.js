import service from '@/utils/request'

// @Tags Package
// @Summary 创建Package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Package true "创建Package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /package/createPackage [post]
export const createPackage = (data) => {
  return service({
    url: '/package/createPackage',
    method: 'post',
    data
  })
}

// @Tags Package
// @Summary 删除Package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Package true "删除Package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /package/deletePackage [delete]
export const deletePackage = (data) => {
  return service({
    url: '/package/deletePackage',
    method: 'delete',
    data
  })
}

// @Tags Package
// @Summary 删除Package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除Package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /package/deletePackage [delete]
export const deletePackageByIds = (data) => {
  return service({
    url: '/package/deletePackageByIds',
    method: 'delete',
    data
  })
}

// @Tags Package
// @Summary 更新Package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Package true "更新Package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /package/updatePackage [put]
export const updatePackage = (data) => {
  return service({
    url: '/package/updatePackage',
    method: 'put',
    data
  })
}

// @Tags Package
// @Summary 用id查询Package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.Package true "用id查询Package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /package/findPackage [get]
export const findPackage = (params) => {
  return service({
    url: '/package/findPackage',
    method: 'get',
    params
  })
}

// @Tags Package
// @Summary 分页获取Package列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取Package列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /package/getPackageList [get]
export const getPackageList = (params) => {
  return service({
    url: '/package/getPackageList',
    method: 'get',
    params
  })
}
