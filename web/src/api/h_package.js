import service from '@/utils/request'

// @Tags H_package
// @Summary 创建H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.H_package true "创建H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /h_package/createH_package [post]
export const createH_package = (data) => {
  return service({
    url: '/h_package/createH_package',
    method: 'post',
    data
  })
}

// @Tags H_package
// @Summary 删除H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.H_package true "删除H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /h_package/deleteH_package [delete]
export const deleteH_package = (data) => {
  return service({
    url: '/h_package/deleteH_package',
    method: 'delete',
    data
  })
}

// @Tags H_package
// @Summary 删除H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /h_package/deleteH_package [delete]
export const deleteH_packageByIds = (data) => {
  return service({
    url: '/h_package/deleteH_packageByIds',
    method: 'delete',
    data
  })
}

// @Tags H_package
// @Summary 更新H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.H_package true "更新H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /h_package/updateH_package [put]
export const updateH_package = (data) => {
  return service({
    url: '/h_package/updateH_package',
    method: 'put',
    data
  })
}

// @Tags H_package
// @Summary 用id查询H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.H_package true "用id查询H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /h_package/findH_package [get]
export const findH_package = (params) => {
  return service({
    url: '/h_package/findH_package',
    method: 'get',
    params
  })
}

// @Tags H_package
// @Summary 分页获取H_package列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取H_package列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /h_package/getH_packageList [get]
export const getH_packageList = (params) => {
  return service({
    url: '/h_package/getH_packageList',
    method: 'get',
    params
  })
}
