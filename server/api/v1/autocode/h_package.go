package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
    "github.com/flipped-aurora/gin-vue-admin/server/model/autocode"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/request"
    autocodeReq "github.com/flipped-aurora/gin-vue-admin/server/model/autocode/request"
    "github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
    "github.com/flipped-aurora/gin-vue-admin/server/service"
    "github.com/gin-gonic/gin"
    "go.uber.org/zap"
)

type H_packageApi struct {
}

var h_packageService = service.ServiceGroupApp.AutoCodeServiceGroup.H_packageService


// CreateH_package 创建H_package
// @Tags H_package
// @Summary 创建H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body autocode.H_package true "创建H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /h_package/createH_package [post]
func (h_packageApi *H_packageApi) CreateH_package(c *gin.Context) {
	var h_package autocode.H_package
	_ = c.ShouldBindJSON(&h_package)
	if err := h_packageService.CreateH_package(h_package); err != nil {
        global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// DeleteH_package 删除H_package
// @Tags H_package
// @Summary 删除H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body autocode.H_package true "删除H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /h_package/deleteH_package [delete]
func (h_packageApi *H_packageApi) DeleteH_package(c *gin.Context) {
	var h_package autocode.H_package
	_ = c.ShouldBindJSON(&h_package)
	if err := h_packageService.DeleteH_package(h_package); err != nil {
        global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// DeleteH_packageByIds 批量删除H_package
// @Tags H_package
// @Summary 批量删除H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /h_package/deleteH_packageByIds [delete]
func (h_packageApi *H_packageApi) DeleteH_packageByIds(c *gin.Context) {
	var IDS request.IdsReq
    _ = c.ShouldBindJSON(&IDS)
	if err := h_packageService.DeleteH_packageByIds(IDS); err != nil {
        global.GVA_LOG.Error("批量删除失败!", zap.Any("err", err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// UpdateH_package 更新H_package
// @Tags H_package
// @Summary 更新H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body autocode.H_package true "更新H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /h_package/updateH_package [put]
func (h_packageApi *H_packageApi) UpdateH_package(c *gin.Context) {
	var h_package autocode.H_package
	_ = c.ShouldBindJSON(&h_package)
	if err := h_packageService.UpdateH_package(h_package); err != nil {
        global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// FindH_package 用id查询H_package
// @Tags H_package
// @Summary 用id查询H_package
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query autocode.H_package true "用id查询H_package"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /h_package/findH_package [get]
func (h_packageApi *H_packageApi) FindH_package(c *gin.Context) {
	var h_package autocode.H_package
	_ = c.ShouldBindQuery(&h_package)
	if err, reh_package := h_packageService.GetH_package(h_package.ID); err != nil {
        global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"reh_package": reh_package}, c)
	}
}

// GetH_packageList 分页获取H_package列表
// @Tags H_package
// @Summary 分页获取H_package列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query autocodeReq.H_packageSearch true "分页获取H_package列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /h_package/getH_packageList [get]
func (h_packageApi *H_packageApi) GetH_packageList(c *gin.Context) {
	var pageInfo autocodeReq.H_packageSearch
	_ = c.ShouldBindQuery(&pageInfo)
	if err, list, total := h_packageService.GetH_packageInfoList(pageInfo); err != nil {
	    global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
        response.FailWithMessage("获取失败", c)
    } else {
        response.OkWithDetailed(response.PageResult{
            List:     list,
            Total:    total,
            Page:     pageInfo.Page,
            PageSize: pageInfo.PageSize,
        }, "获取成功", c)
    }
}
