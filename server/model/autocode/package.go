// 自动生成模板Package
package autocode

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// Package 结构体
// 如果含有time.Time 请自行import time包
type Package struct {
      global.GVA_MODEL
      Name  string `json:"name" form:"name" gorm:"column:name;comment:;type:varchar(20);"`
      Date  string `json:"date" form:"date" gorm:"column:date;comment:yyyy-mm-dd;type:varchar(10);"`
      Count  *int `json:"count" form:"count" gorm:"column:count;comment:套餐剩余数量;type:int"`
      Detail  string `json:"detail" form:"detail" gorm:"column:detail;comment:;type:text(255);"`
}


