import service from '@/utils/request'

// @Tags Result
// @Summary 创建Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Result true "创建Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /result/createResult [post]
export const createResult = (data) => {
  return service({
    url: '/result/createResult',
    method: 'post',
    data
  })
}

// @Tags Result
// @Summary 删除Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Result true "删除Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /result/deleteResult [delete]
export const deleteResult = (data) => {
  return service({
    url: '/result/deleteResult',
    method: 'delete',
    data
  })
}

// @Tags Result
// @Summary 删除Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /result/deleteResult [delete]
export const deleteResultByIds = (data) => {
  return service({
    url: '/result/deleteResultByIds',
    method: 'delete',
    data
  })
}

// @Tags Result
// @Summary 更新Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.Result true "更新Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /result/updateResult [put]
export const updateResult = (data) => {
  return service({
    url: '/result/updateResult',
    method: 'put',
    data
  })
}

// @Tags Result
// @Summary 用id查询Result
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query model.Result true "用id查询Result"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /result/findResult [get]
export const findResult = (params) => {
  return service({
    url: '/result/findResult',
    method: 'get',
    params
  })
}

// @Tags Result
// @Summary 分页获取Result列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data query request.PageInfo true "分页获取Result列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /result/getResultList [get]
export const getResultList = (params) => {
  return service({
    url: '/result/getResultList',
    method: 'get',
    params
  })
}
